package br.edu.ifpi.dao;

import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.edu.ifpi.entity.Automovel;

public class InserirJPA {
	
	public static void main(String[] args) {
		
		Automovel automovel = new Automovel();
		automovel.setChassi("123");
		automovel.setPlaca("ABC-1234");
		automovel.setPreco(10000);
		automovel.setNome("Fiesta 3");
		automovel.setDataVenda(Calendar.getInstance());
		
		
		EntityManagerFactory emf 
		= Persistence.createEntityManagerFactory("jpa-hibernate-mysql-dev");
				
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		
		automovel = em.merge(automovel);
		
		em.getTransaction().commit();
		
		System.out.println("ID " + automovel.getId());
		
		em.close();
		emf.close();
		
	}
	
	

}
