package br.edu.ifpi.dao;

import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class AtualizarBD {
	
	public static void main(String[] args) {
		
		/*
		 * 0 - Mudar hbm2ddl para update
		 * 1 - Criar a F�brica de EntityManage
		 * 2 - Criar o EntityManager
		 * 3 - Close e Close*/
		
		//Passo 0
		Properties props = new Properties();
		props.put("hibernate.hbm2ddl.auto", "create");
		
		//Passo 1
		EntityManagerFactory emf 
		= Persistence.createEntityManagerFactory("jpa-hibernate-mysql-dev", props);
		
		
		emf.close();
		
	}

}
