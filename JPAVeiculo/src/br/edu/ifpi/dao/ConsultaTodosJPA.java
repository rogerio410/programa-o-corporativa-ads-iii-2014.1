package br.edu.ifpi.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.edu.ifpi.entity.Automovel;

public class ConsultaTodosJPA {
	
	public static void main(String[] args) {
		
		EntityManager em = JPAUtil.getCurrentEntityManager();
		
		List<Automovel> lista;
		
		lista = em.createQuery("from Automovel").getResultList();
		
		for (Automovel automovel : lista) {
			System.out.println(automovel);
		}
		
		
	}

}
