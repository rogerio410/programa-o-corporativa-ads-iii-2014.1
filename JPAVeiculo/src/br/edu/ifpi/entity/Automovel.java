package br.edu.ifpi.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Automovel {
	
	@Id 
	@GeneratedValue
	private Long id;
	
	@Column(length=8)
	private String placa;
	
	@Column(nullable=false)
	private String chassi;
	
	@Column(length=50)
	private String nome;
	private String montadora;
	private double preco;
	private Calendar dataVenda;
	private String cor;
	
	public Automovel() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getChassi() {
		return chassi;
	}

	public void setChassi(String chassi) {
		this.chassi = chassi;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMontadora() {
		return montadora;
	}

	public void setMontadora(String montadora) {
		this.montadora = montadora;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public Calendar getDataVenda() {
		return dataVenda;
	}

	public void setDataVenda(Calendar dataVenda) {
		this.dataVenda = dataVenda;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	@Override
	public String toString() {
		return "Automovel [id=" + id + ", placa=" + placa + ", chassi="
				+ chassi + ", nome=" + nome + ", montadora=" + montadora
				+ ", preco=" + preco + ", dataVenda=" + dataVenda + ", cor="
				+ cor + "]";
	}
	

}
