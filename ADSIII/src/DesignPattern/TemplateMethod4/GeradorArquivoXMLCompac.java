package DesignPattern.TemplateMethod4;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.omg.CORBA.BAD_CONTEXT;

public class GeradorArquivoXMLCompac {
	
	String conteudo = null;
	int delay = 1;
	private String extensao = ".zip";
	
	public GeradorArquivoXMLCompac() {
	}
	
	public final void gerarArquivo(String filename, Map<String, Object> propriedades)
			throws IOException{
		
		StringBuilder propFileBuilder = null;
		propFileBuilder = new StringBuilder();
		
		//Obter conte�do: XML
		propFileBuilder.append("<properties>");
		
		for (String prop : propriedades.keySet()) {
			propFileBuilder
				.append("<"+prop+">"+propriedades.get(prop)+"</"+prop+">");
		}
		
		propFileBuilder.append("</properties>");
		
		this.conteudo = propFileBuilder.toString();
		
		byte[] bytes = conteudo.getBytes();
		
		//P�s-Processar: Compactar
		ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
		ZipOutputStream out = new ZipOutputStream(byteOut);
		out.putNextEntry(new ZipEntry("arquivo_interno.xml"));
		out.write(bytes);
		out.closeEntry();
		out.close();
		
		bytes = byteOut.toByteArray();
		
		//Finalizar: gravar arquivo com o conte�do gerado e processado
		FileOutputStream fileout = new FileOutputStream(filename+extensao );
		fileout.write(bytes);
		fileout.close();
	}
	
}
