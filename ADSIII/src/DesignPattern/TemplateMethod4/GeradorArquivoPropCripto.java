package DesignPattern.TemplateMethod4;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.omg.CORBA.BAD_CONTEXT;

public class GeradorArquivoPropCripto {
	
	String conteudo = null;
	int delay = 1;
	private String extensao = ".txt";
	
	public GeradorArquivoPropCripto() {
	}
	
	//Template
	public final void gerarArquivo(String filename, Map<String, Object> propriedades)
			throws IOException{
		
		
		StringBuilder propFileBuilder = null;
		propFileBuilder = new StringBuilder();
		
		//Obter conte�do: Prop (chave=valor)
		for (String prop : propriedades.keySet()) {
			propFileBuilder.append(prop + "=" + propriedades.get(prop) + "\n");
		}
		
		this.conteudo = propFileBuilder.toString();
		
		byte[] bytes = conteudo.getBytes();
		
		
		//P�s-Processar: Criptografar
		byte[] newBytes = new byte[bytes.length];
		for (int i = 0; i < bytes.length; i++) {
			newBytes[i] = (byte) ((bytes[i] + delay) % Byte.MAX_VALUE);
		}
		
		bytes = newBytes;
		
		//Finalizar: gravar arquivo com o conte�do gerado e processado
		FileOutputStream fileout = new FileOutputStream(filename+extensao);
		fileout.write(bytes);
		fileout.close();
	}
	
}
