package DesignPattern.TemplateMethod4;

import java.util.HashMap;
import java.util.Map;

public class PrincipalOriginal {
	
	public static void main(String[] args) {
		
		Map<String, Object> propriedades = new HashMap();
		
		propriedades.put("nome", "Sistema de Compartilhameto de Pre�os");
		propriedades.put("plataforma", "Android");
		propriedades.put("autor", "joaozim");
		
		//GeradorArquivoXMLCompac gerador = new GeradorArquivoXMLCompac();
		GeradorArquivoPropCripto gerador = new GeradorArquivoPropCripto();
		
		try {
			gerador.gerarArquivo("erick", propriedades);
			System.out.println("OK.");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}

}
