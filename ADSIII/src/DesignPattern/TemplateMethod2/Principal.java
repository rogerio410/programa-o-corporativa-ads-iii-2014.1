package DesignPattern.TemplateMethod2;

import java.util.HashMap;
import java.util.Map;

public class Principal {
	
	public static void main(String[] args) {
		
		Map<String, Object> propriedades = new HashMap();
		
		propriedades.put("nome", "Sistema de Compartilhameto de Pre�os");
		propriedades.put("plataforma", "Android");
		propriedades.put("autor", "joaozim");
		
		GeradorArquivo gerador = new GeradorArquivo("XMLCompactado", 1);
		
		try {
			gerador.gerarArquivo("propXMLZIP.zip", propriedades);
			System.out.println("OK.");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}

}
