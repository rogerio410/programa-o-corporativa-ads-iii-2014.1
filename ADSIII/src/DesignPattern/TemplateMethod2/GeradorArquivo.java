package DesignPattern.TemplateMethod2;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.omg.CORBA.BAD_CONTEXT;

public class GeradorArquivo {
	
	String tipo_arquivo = "XMLCompactado"; // or "PropCriptografado"
	String conteudo = null;
	int delay = 1;
	
	public GeradorArquivo(String tipo_arquivo, int delay) {
		this.tipo_arquivo = tipo_arquivo;
		this.delay = delay;
	}
	
	//Template
	public void gerarArquivo(String nome, Map<String, Object> propriedades)
			throws IOException{
		
		
		//Obter conte�do: XML ou Prop (chave=valor)
		StringBuilder propFileBuilder = null;
		propFileBuilder = new StringBuilder();
		
		if (tipo_arquivo.equals("XMLCompactado")){
			propFileBuilder.append("<properties>");
			
			for (String prop : propriedades.keySet()) {
				propFileBuilder
					.append("<"+prop+">"+propriedades.get(prop)+"</"+prop+">");
			}
			
			propFileBuilder.append("</properties>");
			
		}else { // ou Props
			
			for (String prop : propriedades.keySet()) {
				propFileBuilder.append(prop + "=" + propriedades.get(prop) + "\n");
			}
			
		}
		
		this.conteudo = propFileBuilder.toString();
		
		byte[] bytes = conteudo.getBytes();
		
		
		//P�s-Processar
		if (tipo_arquivo.equals("XMLCompactado")){
			//Compactar
			ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
			ZipOutputStream out = new ZipOutputStream(byteOut);
			out.putNextEntry(new ZipEntry("internal"));
			out.write(bytes);
			out.closeEntry();
			out.close();
			
			bytes = byteOut.toByteArray();
		}else {
			
			//Criptografar 
			byte[] newBytes = new byte[bytes.length];
			for (int i = 0; i < bytes.length; i++) {
				newBytes[i] = (byte) ((bytes[i] + delay) % Byte.MAX_VALUE);
			}
			
			bytes = newBytes;
		}
		
		
		//Finalizar: gravar arquivo com o conte�do gerado e processado
		FileOutputStream fileout = new FileOutputStream(nome);
		fileout.write(bytes);
		fileout.close();
	}
	
}
