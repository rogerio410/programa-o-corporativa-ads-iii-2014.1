package DesignPattern.TemplateMethod1;

import java.util.HashMap;
import java.util.Map;


public class Principal {

	public static void main(String[] args) {

		Map<String, Object> propriedades = new HashMap();

		propriedades.put("nome", "Sistema de Compartilhameto de Pre�os");
		propriedades.put("plataforma", "Android");
		propriedades.put("autor", "joaozim");

		GeradorArquivo gerador = new GeradorArquivo("XMLCompactado11");

		try {
			System.out.println("Processo iniciado.");
			gerador.gerarArquivo("arquivo.rog", propriedades);
			System.out.println("Processo finalizado.");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

}
