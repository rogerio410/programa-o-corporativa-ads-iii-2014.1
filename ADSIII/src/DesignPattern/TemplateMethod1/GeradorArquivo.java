package DesignPattern.TemplateMethod1;

import java.io.IOException;
import java.util.Map;

public class GeradorArquivo {
	
	String tipo_arquivo = "XMLCompactado"; // or "PropCriptografado"
	String conteudo = null;
	
	public GeradorArquivo(String tipo_arquivo) {
		this.tipo_arquivo = tipo_arquivo;
	}
	
	public void gerarArquivo(String nome, Map<String, Object> propriedades)
			throws IOException{
		
		//Obter conte�do: XML ou Prop (chave=valor)
		if (tipo_arquivo.equals("XMLCompactado")){
			conteudo = (" XML Gerado" );
		}else { // ou Props
			conteudo = (" PROP Gerado");
		}
		
		System.out.println("Conteudo Gerado: "+ conteudo);
		
		//P�s-Processar
		if (tipo_arquivo.equals("XMLCompactado")){
			//Compactar
			conteudo =  "Compactado --> " + conteudo;
		}else {
			//Criptografar 
			conteudo = "Criptografado --> " + conteudo;
		}
		
		System.out.println("Pos-processamento: " + conteudo);
		
		//Finalizar: gravar arquivo com o conte�do gerado e processado
		System.out.println("Arquigo "+ nome +" ( " + conteudo + ")");
	}

}
