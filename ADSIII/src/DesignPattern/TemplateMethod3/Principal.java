package DesignPattern.TemplateMethod3;

import java.util.HashMap;
import java.util.Map;

public class Principal {
	
	public static void main(String[] args) {
		
		Map<String, Object> propriedades = new HashMap();
		
		propriedades.put("nome", "Sistema de Compartilhameto de Pre�os");
		
		//GeradorArquivo geradorXML = new GeradorXMLCompactado();
		GeradorArquivo geradorArq = new GeradorPropriedadesCriptografado(10);
		
		try {
			geradorArq.gerarArquivo("arquivo.rog", propriedades);
			System.out.println("OK.");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}

}
